package com.example;



import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public enum Produit {
    CAFE_LONG("Café long", TypeMachine.MACHINE_A_CAFE, "src\\main\\java\\com\\example\\cafe_long.jpeg"),
    EXPRESSO("Expresso corsé", TypeMachine.MACHINE_A_CAFE,"src\\main\\java\\com\\example\\corse.jpg"),
    CAFE_AU_LAIT("Café au lait", TypeMachine.MACHINE_A_CAFE,"src\\main\\java\\com\\example\\cafe_lait.jpg"),
    CAPPUCINO("Cappucino", TypeMachine.MACHINE_A_CAFE,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    CHOCOLAT_CHAUD("Chocolat chaud", TypeMachine.MACHINE_A_CAFE, "src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    LATTE_MACCHIATO("Latte macchiato", TypeMachine.MACHINE_A_CAFE,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),

    M_NMS("M&M's", TypeMachine.DISTRIBUTEUR,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    JUS_D_ORANGE("Jus d'orange", TypeMachine.DISTRIBUTEUR, "src\\main\\java\\com\\example\\jus_fruits.png"),
    HAWAI("Hawai à l'ananas", TypeMachine.DISTRIBUTEUR,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    COCA_COLA("Coca cola", TypeMachine.DISTRIBUTEUR,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    EAU_PLATE("Eau plate", TypeMachine.DISTRIBUTEUR,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    EAU_GAZEUSE("Eau gazeuse", TypeMachine.DISTRIBUTEUR,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    JUS_DE_FRUITS("Jus de fruits", TypeMachine.DISTRIBUTEUR,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png"),
    SANDWICHES("Sandwiches", TypeMachine.DISTRIBUTEUR,"src\\main\\java\\com\\example\\ClaquetteuuuH_with_a_gigachad_body_2f206ac9-71f6-46d1-bd45-dee5b21da393.png");

    private String caption;
    private TypeMachine typeMachine;
    private ImageIcon imageIcon;

    Produit(String caption, TypeMachine typeMachine) {
        this.caption = caption;
        this.typeMachine = typeMachine;
    }

    Produit(String caption, TypeMachine typeMachine, String imagePath) {
        this.caption = caption;
        this.typeMachine = typeMachine;
        this.imageIcon = new ImageIcon(imagePath);
    }

    public String getCaption() {
        return caption;
    }

    public TypeMachine getTypeMachine() {
        return typeMachine;
    }

    public ImageIcon getImageIcon() {
        return imageIcon;
    }

    public static List<Produit> getProduitsFromTypeMachine(TypeMachine type) {
        List<Produit> listeProd = new ArrayList<>();

        for (Produit produit : values()) {
            if (produit.typeMachine == type) {
                listeProd.add(produit);
            }
        }
        return listeProd;
    }

    public void setImagePath(String imagePath) {
        this.imageIcon = new ImageIcon(imagePath);
    }
}
