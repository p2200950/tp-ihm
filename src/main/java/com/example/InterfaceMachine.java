package com.example;

import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import javax.swing.*;

public class InterfaceMachine extends JFrame {
	
	protected Machine distributeur;
	private JButton commander;
	private JLabel position;
	private Produit p;
	private List<JLabel> produitLabels = new ArrayList<>();
	
	public InterfaceMachine(Machine distributeur) {
		super();
		
		this.distributeur = distributeur;
		setTitle(distributeur.getName());
		setPreferredSize(new Dimension(1000, 500));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents();
		this.pack();
		setVisible(true);
	}
	
	public void setPosition(JLabel position) {
		this.position = position;
	}
	
	public void initComponents() {
		JPanel cp = new JPanel();
          cp.setLayout(new BorderLayout());
		cp.setBorder(BorderFactory.createTitledBorder("panel produits"));
		JPanel panelInfo = new JPanel();
          panelInfo.setBorder(BorderFactory.createTitledBorder("panel info positions"));
		
		this.commander = new javax.swing.JButton("Commander");
		// position button in left middle
		
		this.position = new JLabel("Position : " + distributeur.getPosition());
          panelInfo.setLayout(new BorderLayout());
		panelInfo.add(commander, BorderLayout.NORTH);
		panelInfo.add(position, BorderLayout.SOUTH);
		cp.add(panelInfo, BorderLayout.NORTH);
		
		JLabel produitlabel = new JLabel("\n Liste des produits :");
		JPanel panelProduits = new JPanel();
          panelProduits.setBorder(BorderFactory.createTitledBorder("produits listes"));
		
		panelProduits.setLayout(new BoxLayout(panelProduits, BoxLayout.Y_AXIS));
		
		panelProduits.add(produitlabel);
		
		for (Produit prod : distributeur.getProducts()) {
			String p = prod.getCaption();
			Integer quantite = this.distributeur.getStock(prod);
			
			produitlabel = new JLabel("Nom du produit : " + p + " " + "\n Quantité du produit : " + quantite);
			panelProduits.add(produitlabel);
			// panelProduits.setBorder(borderFacoty());
		}
		
		cp.add(panelProduits, BorderLayout.SOUTH);
		add(cp,BorderLayout.WEST);
		
		
		pack();
	}
	
	public JButton getCommander() {
		return this.commander;
	}
	
}
