package com.example;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JSeparator;


public class InterfaceMachineACafe extends InterfaceMachine {
    
    private JButton sugarButton, rechargeStockGoblets, changeImageArticle;
    private JLabel sugarLabel, labelProd, imageLabel, prodIndisponible;
    private JComboBox<String> comboBox;
    private JPanel panelSucre,panelGobelet,sousPanelImage;
    private JRadioButton radioButton;
    private JProgressBar loadingBar;
    private Timer loadingTimer;
    private Integer loadingPourcentage;
    
    public InterfaceMachineACafe(MachineACafe machineCafe) {
        super(machineCafe);
        pack();
    }
    
    @Override
    public void initComponents() {
        super.initComponents();
        JPanel cp = (JPanel) getContentPane();
        panelSucre = new JPanel();
         panelGobelet = new JPanel();
        panelGobelet.setBorder(BorderFactory.createTitledBorder("panel Gobelet"));
        JPanel panelComboBox = new JPanel();
         sousPanelImage = new JPanel();
        sousPanelImage.setBorder(BorderFactory.createTitledBorder("sous panel "));
        sousPanelImage.setLayout(new FlowLayout());
        panelComboBox.setBorder(BorderFactory.createTitledBorder("Combo Box"));
        
        sugarButton = new JButton("Sélectionner le sucre");
        sugarLabel = new JLabel("Sucre");
        rechargeStockGoblets = new JButton("Recharger Gobelets");
        labelProd = new JLabel("Stock de gobelet");
        
       
        
        panelSucre.add(sugarButton);
        panelSucre.setBorder(BorderFactory.createTitledBorder("info menu "));
        
        panelSucre.add(sugarLabel);
       
        panelSucre.setLayout(new BoxLayout(panelSucre, BoxLayout.X_AXIS));
        
        comboBox = new JComboBox<>();
        for (Produit p : getMachineAcafe().getProducts()) {
            String prod = p.getCaption();
            comboBox.addItem(prod);
        }
        
        changeImageArticle = new JButton("Changer l'image du produit");
        
        sousPanelImage.add(comboBox);
       
        cp.add(panelSucre, BorderLayout.NORTH);
        cp.add(panelGobelet,BorderLayout.SOUTH);
        cp.add(panelComboBox,BorderLayout.EAST);
        labelProd.setText(String.valueOf(getMachineAcafe().getStockGobelet()));
       
        ImageIcon imageIcon = new ImageIcon("src/main/java/com/example/cafe_long.jpeg");
        Image image = imageIcon.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        ImageIcon taille = new ImageIcon(image);
        imageLabel = new JLabel(taille);
        JPanel panelImage = new JPanel(new FlowLayout());
        panelImage.add(imageLabel);
        panelImage.setBorder(BorderFactory.createTitledBorder("image "));
        panelComboBox.setLayout(new BorderLayout());
        panelComboBox.add(sousPanelImage,BorderLayout.NORTH);
        panelComboBox.add(panelImage,BorderLayout.SOUTH);
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Fenêtre");
        JMenuItem fermer = new JMenuItem("Fermer la fenêtre");
        fermer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose(); // Ferme la fenêtre
            }
        });
        menu.add(fermer);
        menuBar.add(menu);
        
        JMenu maint = new JMenu("Maintenance");
        JMenuItem maintenancePanel = new JMenuItem("Maintenance");
        JPanel maintenancePanelContent = new JPanel();
        radioButton = new JRadioButton("Maintenance");
     
   radioButton.addActionListener(new ActionListener() {
       @Override
       public void actionPerformed(ActionEvent e) {
           
           if(radioButton.isSelected()) {
               panelGobelet.add(rechargeStockGoblets, BorderLayout.NORTH);
               panelGobelet.add(labelProd, BorderLayout.SOUTH);
               sousPanelImage.add(changeImageArticle);
               rechargeStockGoblets.setVisible(true);
               labelProd.setVisible(true);
               changeImageArticle.setVisible(true);
               
           }
           else {
               rechargeStockGoblets.setVisible(false);
               labelProd.setVisible(false);
               changeImageArticle.setVisible(false);
           }
           revalidate();
       }
   });
        
        maintenancePanel.add(radioButton);
        maintenancePanel.add(maintenancePanelContent);
        maint.add(maintenancePanel);
        menuBar.add(maint);
        
        setJMenuBar(menuBar);
        loadingBar = new JProgressBar(0, 100);
        initEventListeners();
    }
    
    public void updateLabelSugar(Boolean v) {
        if (v == null) {
            return;
        } else if (v) {
            sugarLabel.setText("Sucre : Oui");
         
        } else {
            sugarLabel.setText("Sucre : Non");
        }
        pack();
    }
    
    public MachineACafe getMachineAcafe() {
        return (MachineACafe) this.distributeur;
    }
    
    public void initEventListeners() {
        sugarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean choix;
                String message = "Voulez-vous du sucre ?";
                int reponse = JOptionPane.showConfirmDialog(null, message, "Sucre", JOptionPane.YES_NO_CANCEL_OPTION);
                if (reponse == JOptionPane.YES_OPTION) {
                    choix = true;
                } else {
                    choix = false;
                }
                updateLabelSugar(choix);
            }
        });
        
        rechargeStockGoblets.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String respGoblets = JOptionPane.showInputDialog(null, "Combien ?", "Rechargement de gobelets", JOptionPane.PLAIN_MESSAGE);
                try {
                    int value = Integer.parseInt(respGoblets);
                    if (value > 0 && value < Integer.MAX_VALUE) {
                        getMachineAcafe().fillGobelets(value);
                        labelProd.setText(String.valueOf(getMachineAcafe().getStockGobelet()));
                    } else {
                        JOptionPane.showMessageDialog(null, "Veuillez entrer un nombre positif.", "Erreur de saisie", JOptionPane.ERROR_MESSAGE);
                    }
                }catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Veuillez entrer un nombre valide.", "Erreur de saisie", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        getCommander().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String msg = "Commande : " + comboBox.getSelectedItem().toString();
                System.out.println(msg);
            }
        });
        
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedProduct = (String) comboBox.getSelectedItem();
                for (Produit produit : getMachineAcafe().getProducts()) {
                    if (produit.getCaption().equals(selectedProduct)) {
                        ImageIcon imageIcon = new ImageIcon(produit.getImageIcon().getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH));
                        if (imageIcon != null) {
                            imageLabel.setIcon(imageIcon);
                        }
                        break;
                    }
                }
            }
        });
        
        changeImageArticle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ChangeImage();
            }
        });
    }
    
    public void ChangeImage() {
        JFileChooser fileChooser = new JFileChooser();
        
        // Définir le répertoire de départ
        File initialDirectory = new File("src\\main\\java\\com\\example\\");
        fileChooser.setCurrentDirectory(initialDirectory);
        
        fileChooser.setFileFilter(new FileNameExtensionFilter("Images", "jpg", "jpeg", "png", "gif")); // Filtrer les fichiers par extension
        
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            String imagePath = selectedFile.getAbsolutePath();
            String selectedProduct = (String) comboBox.getSelectedItem();
            
            for (Produit produit : getMachineAcafe().getProducts()) {
                if (produit.getCaption().equals(selectedProduct)) {
                    produit.setImagePath(imagePath);
                    ImageIcon imageIcon = new ImageIcon(imagePath);
                    Image scaledImage = imageIcon.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
                    imageIcon = new ImageIcon(scaledImage);
                    imageLabel.setIcon(imageIcon);
                    pack();
                    break;
                }
            }
        }
    }
    
    
}
