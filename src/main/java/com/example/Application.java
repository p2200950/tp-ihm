package com.example;

import java.util.List;


public class Application {

    public static void main(String[] args) {

        List<Produit> produitsDistributeurs = Produit.getProduitsFromTypeMachine(TypeMachine.DISTRIBUTEUR);
        List<Produit> produitsmachineCafes = Produit.getProduitsFromTypeMachine(TypeMachine.MACHINE_A_CAFE);

        MachineACafe distributeur = new MachineACafe("Distributeur de Rafik", Position.DEPT_INFO, produitsDistributeurs);


        MachineACafe machineACafe = new MachineACafe("Machine de Rafik", Position.DEPT_CHIMIE, produitsmachineCafes);
        InterfaceMachineACafe interMachine = new InterfaceMachineACafe(machineACafe);
        InterfaceMachineACafe in = new InterfaceMachineACafe(distributeur);

    }

}
