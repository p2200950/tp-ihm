package com.example;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author p2200950
 */
import java.util.*;
import javax.swing.JComponent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Machine {

    private String name;
    private Position position;

    private Map<Produit, Integer> stock;

    // la map contiendra les produits avec leur quantité
    Machine(String nom, Position p, List<Produit> produits) {
        this.name = nom;
        this.position = p;
        this.stock = new HashMap();
        for (Produit produit : produits) {
            this.stock.put(produit, 10);

        }
    }

    public String getPosition() {
        return this.position.toString();
    }

    public Set<Produit> getProducts() {

        return this.stock.keySet();

    }

    public int getStock(Produit prod) {

        return this.stock.get(prod);
    }

    public boolean order(Produit prod) {
        int s = this.stock.get(prod);
        boolean verife = false;
        if (s > 0) {
            this.stock.put(prod, s--);
            verife = true;
        }
        return verife;
    }

    public void fill(Produit prod, int quantite) {
        int s = this.stock.get(prod);
        System.out.println(s);

        if (s >= 0) {
            this.stock.put(prod, s + quantite);

        }

        System.out.println(this.stock.get(prod));
    }

    public String getName() {
        return name;
    }
}
