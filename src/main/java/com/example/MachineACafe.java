package com.example;

import java.util.*;
import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MachineACafe extends Machine {

    protected Integer stockGobelet;

    MachineACafe(String nom, Position p, List<Produit> produits) {

        super(nom, p, produits);
        this.stockGobelet = 10;

    }

    @Override
    public int getStock(Produit prod) {
        int min = Integer.MAX_VALUE;
        if (this.stockGobelet < super.getStock(prod)) {
            min = this.stockGobelet;
        } else {
            min = super.getStock(prod);
        }
        return min;
    }

    public boolean order(Produit prod) {
        boolean commandeValide = false;

        if (this.stockGobelet > 0) {
            commandeValide = true;
            super.order(prod);
        }
        return commandeValide;
    }

    public void fillGobelets(int nbGobelets) {

        this.stockGobelet += nbGobelets;

    }

    public Integer getStockGobelet() {
        return stockGobelet;
    }

}
