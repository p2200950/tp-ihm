package com.example;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class YesNoModal extends JFrame {


    private JLabel label;
    private JButton ButtonYes, ButtonNo, ButtonCancel;
    private Boolean result;
    private String labelText;

    YesNoModal(String lblText) {
        this.labelText = lblText;

        this.result = null;


        initComponents();
        initEventListeners();
        this.pack();
        setVisible(true);
    }

    public void initComponents() {

        this.label = new JLabel(this.labelText);
        this.ButtonYes = new JButton("Yes");
        this.ButtonNo = new JButton("No");
        this.ButtonCancel = new JButton("Annuler");

        JPanel panelLabel = new JPanel();
        panelLabel.add(this.label);

        JPanel panelBoutons = new JPanel();
        panelBoutons.add(this.ButtonYes);
        panelBoutons.add(this.ButtonNo);
        panelBoutons.add(this.ButtonCancel);

        JPanel conteneurPrincipal = (JPanel) getContentPane();
        conteneurPrincipal.add(panelLabel, BorderLayout.NORTH);
        conteneurPrincipal.add(panelBoutons, BorderLayout.SOUTH);


    }


    public void initEventListeners() {

        ButtonYes.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                result = true;
                setVisible(false);
                dispose();
            }


        });
        ButtonNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                result = false;
                setVisible(false);
                dispose();
            }
        });
        ButtonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
            }
        });


    }

    public Boolean getResult() {
        return result;
    }


}


